package com.example.bpmobileaplication

/*
    EditOrSavePilotActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita pro editaci a tvorbu pilotů majitelem
*/

import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import java.io.IOException
import java.text.SimpleDateFormat

class EditOrSavePilotActivity : AppCompatActivity() {

    // Pokud je tato hodnota nastavena, tak jde o editaci uživatele,
    // jinak o tvorbu nového uživatele
    var user_ID = 0

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_or_save_pilot_layout)

        // Zjištění jestli jde o úpravu a nebo tvorbu nevého pilota
        user_ID = intent.getIntExtra("USER_ID", 0)

        // Odstranění symbolů spinneru které už jsou používány
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        // Funguje i pro tvorbu nových pilotů protože admin má id 0
        var notUsedSymbols = resources.getStringArray(R.array.characters).toList()
        val cursor = db.rawQuery("""SELECT * FROM User WHERE  Role='pilot' AND ID != $user_ID""", null)
        if(cursor.moveToFirst()){
            do{
               notUsedSymbols = notUsedSymbols.filterIndexed{i, _ -> i !== notUsedSymbols.indexOf(cursor.getString(cursor.getColumnIndex("Symbol")))}
            } while (cursor.moveToNext())
        }
        db.close()

        // Načtení dat do spinneru
        val characterSpinner = findViewById<Spinner>(R.id.symbol)
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, notUsedSymbols)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        characterSpinner.adapter = spinnerAdapter

        // nastavení počáteční hodnoty
        if(user_ID != 0){
            // Vyplnění hodnot o editovaném pilotovy
            val dbHelper = FeedReaderDbHelper(this)
            val db = dbHelper.readableDatabase

            val cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$user_ID""", null)
            cursor.moveToFirst()

            var symbol = cursor.getString(cursor.getColumnIndex("Symbol"))
            characterSpinner.setSelection(notUsedSymbols.indexOf(symbol))

            // Nastavení výchozích hodnot do ostatních prvků
            // Heslo ne, bylo by to divný
            var name = cursor.getString(cursor.getColumnIndex("Name"))

            val nameEditText = findViewById<EditText>(R.id.name)
            nameEditText.setText(name)

            db.close()
        }

        // Nastavení liseneru
        val saveButton = findViewById<Button>(R.id.save_button)
        saveButton.setOnClickListener{ saveData() }
    }

    // Uložení zadaných dat a rozdělení na editaci uživatele a tvorbu nového uživatele
    private fun saveData(){
        // Získání hodnot z prvků layoutu
        val nameEditText = findViewById<EditText>(R.id.name)
        val passwordEditText = findViewById<EditText>(R.id.password)
        val symbolSpinner = findViewById<Spinner>(R.id.symbol)

        val name = nameEditText.text.toString()
        val password = passwordEditText.text.toString()
        var symbol = symbolSpinner.selectedItem.toString();

        Log.i("insert", "" + name)
        Log.i("insert", "" + password)
        Log.i("insert", "" + symbol)

        // Kontrola dat
        // Jméno a symbol musí být unikátní
        if(user_ID == 0){
            // Při tvorbě uživatele musí být vyplněné i heslo
            if (password == ""){
                Toast.makeText(this, "Heslo nesmí být prázdné", Toast.LENGTH_SHORT).show()
                return
            }
        }

        if (name == ""){
            Toast.makeText(this, "Jméno nesmí být prázdné", Toast.LENGTH_SHORT).show()
            return
        }

        var dbHelper = FeedReaderDbHelper(this)
        var db = dbHelper.readableDatabase

        // Rozdělení kontroly ůdajů
        // U nového uživatele není potřeba dělat vyjímku na aktuální hodnoty v databázi
        if (user_ID == 0){
            var cursor = db.rawQuery("""SELECT * FROM User WHERE Name='$name'""", null)

            if (cursor.count != 0){
                Toast.makeText(this, "Jméno je již používáno", Toast.LENGTH_SHORT).show()
                db.close()
                return
            }

            cursor = db.rawQuery("""SELECT * FROM User WHERE Symbol='$symbol'""", null)
            if (cursor.count != 0) {
                Toast.makeText(this, "Symbol je už používaný", Toast.LENGTH_SHORT).show()
                db.close()
                return
            }
        }
        else{
            var cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$user_ID""", null)
            cursor.moveToFirst()

            var actualName = cursor.getString(cursor.getColumnIndex("Name"))
            var actualSymbol = cursor.getString(cursor.getColumnIndex("Symbol"))

            if(actualName != name){
                cursor = db.rawQuery("""SELECT * FROM User WHERE Name='$name'""", null)
                if (cursor.count != 0){
                    Toast.makeText(this, "Jméno je již používáno", Toast.LENGTH_SHORT).show()
                    db.close()
                    return
                }
            }

            if(actualSymbol != symbol){
                cursor = db.rawQuery("""SELECT * FROM User WHERE Symbol='$symbol'""", null)
                if (cursor.count != 0) {
                    Toast.makeText(this, "Symbol je už používaný", Toast.LENGTH_SHORT).show()
                    db.close()
                    return
                }
            }
        }

        db.close()

        // Upravení hodnot na zařízení
        var message = if(user_ID == 0){
            "Create User Values Name:$name~Password:$password~Symbol:$symbol~"
        }
        else{
            if(password == ""){
                "Edit User ID:$user_ID~ Values Name:$name~Symbol:$symbol~"
            } else{
                "Edit User ID:$user_ID~ Values Name:$name~Password:$password~Symbol:$symbol~"
            }
        }

        try {
            m_bluetoothSocket!!.outputStream.write(message.toByteArray())

            val buffer = ByteArray(1024)
            var bytes = m_bluetoothSocket!!.inputStream.read(buffer)
            var responce = String(buffer, 0, bytes)

            if (responce != "OK"){
                Toast.makeText(this, "Akce se nepodařila", Toast.LENGTH_SHORT).show()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "Spojení přerušeno, záznam nelze upravovat", Toast.LENGTH_SHORT).show()
            return
        }

        // Upravení hodnot ve vnitřní databázi
        dbHelper = FeedReaderDbHelper(this)
        db = dbHelper.writableDatabase

        if (user_ID == 0){
            db.execSQL("""INSERT INTO User (Name, Password, Role, Symbol) VALUES ('$name', '$password', 'pilot' ,'$symbol')""")
        }
        else{
            if(password == ""){
                db.execSQL("""UPDATE User SET Name='$name', Symbol='$symbol' WHERE ID=$user_ID""")
            }
            else{
                db.execSQL("""UPDATE User SET Name='$name', Password='$password', Symbol='$symbol' WHERE ID=$user_ID""")
            }
        }

        // Nastacení ID nově vytvořeného uživatele
        // Při dalších úpravách se už bude jednat o editaci
        if(user_ID == 0){
            var cursor = db.rawQuery("""SELECT * FROM User WHERE Name='$name'""", null)
            cursor.moveToFirst()

            user_ID = cursor.getInt(cursor.getColumnIndex("ID"))
        }

        db.close()

        Toast.makeText(this, "Pilot uložen", Toast.LENGTH_SHORT).show()
    }
}

// end EditOrSavePilotActivity.kt
