package com.example.bpmobileaplication

/*
    EditPilotsActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita pro prohlížení vytvořených účtů pilotů
*/

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.example.bpmobileaplication.LoginActivity.UserData.userID
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import java.io.IOException

class EditPilotsActivity : AppCompatActivity() {

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_pilots_layout)

        // Nastavení liseneru
        val createButton = findViewById<Button>(R.id.create_pilot)
        createButton.setOnClickListener{ createPilot() }

        // Načtení dat do seznamu
        loadData()
    }

    // Přenačtení dat v listu po vytvoření nového uživatele
    override fun onRestart() {
        super.onRestart()

        loadData()
    }

    // Načtení dat z databáze a zobrazení do listu
    private fun loadData(){
        var usersListText = ArrayList<String>()
        var usersListID = ArrayList<Int>()

        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        val cursor = db.rawQuery("""SELECT * FROM User""", null)
        if(cursor.moveToFirst()){
            do{
                var ID = cursor.getInt(cursor.getColumnIndex("ID"))
                var name = cursor.getString(cursor.getColumnIndex("Name"))

                // Vynechání majitele, nemůže se smazat a je jen jeden
                if(ID != userID){
                    usersListText.add(name)
                    usersListID.add(ID)
                }
            } while (cursor.moveToNext())
        }

        db.close()

        // Načtení dat do listu přes adapter a nastavení lisenerů
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, usersListText)

        val usersList = findViewById<ListView>(R.id.pilots_list)
        usersList.adapter = adapter
        usersList.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            // Zobrazení editace konkrétního uživatele
            val newActivity = Intent(this, EditOrSavePilotActivity::class.java)
            newActivity.putExtra("USER_ID", usersListID[position]) // ID vybraneho uživatele
            startActivity(newActivity)
        }

        usersList.onItemLongClickListener =  AdapterView.OnItemLongClickListener { _, _, position, _ ->
            // Zobrazení okna pro smazání uživatele
            val builder = AlertDialog.Builder(this)
            builder.setMessage("Opravdu chcete smazat pilota?")
                    .setCancelable(false)
                    .setPositiveButton("Ano") { dialog, id ->
                        // Smazání uživatele na zařízení
                        val user_ID = usersListID[position]
                        var message = "Delete User ID:$user_ID"

                        // Kontrola jestli se zařízení neodpojilo
                        try {
                            m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                            val buffer = ByteArray(1024)
                            var bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                            var responce = String(buffer, 0, bytes)

                            if (responce != "OK"){
                                Toast.makeText(this, "Pilota se nepodařilo smazat", Toast.LENGTH_SHORT).show()
                            }
                        } catch (e: IOException) {
                            e.printStackTrace()
                            Toast.makeText(this, "Spojení přerušeno, pilota nelze smazat", Toast.LENGTH_SHORT).show()

                            return@setPositiveButton
                        }

                        // Smazání uživatele v interní databázi
                        val dbHelper = FeedReaderDbHelper(this)
                        val db = dbHelper.writableDatabase

                        db.execSQL("""PRAGMA foreign_keys = ON""")
                        db.execSQL("""DELETE FROM User WHERE ID=$user_ID""")
                        db.close()

                        // Přenačtení dat
                        loadData()
                    }
                    .setNegativeButton("Ne") { dialog, id ->
                        // Dismiss the dialog
                        dialog.dismiss()
                    }
            val alert = builder.create()
            alert.show()

            true
        }
    }

    // Vytvoření nového pilota
    private fun  createPilot(){
        val newActivity = Intent(this, EditOrSavePilotActivity::class.java)
        startActivity(newActivity)
    }

    // Zobrazení menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(LoginActivity.UserData.role == "owner"){
            menuInflater.inflate(R.menu.menu_owner, menu)
        }
        else {
            menuInflater.inflate(R.menu.menu_normal, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    // Akce pro jedlotlivé prvky v menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.records -> {
                //Navrácení na přehled letů
                val newActivity = Intent(this, FlightsActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.editPilots -> {
                // Aktuální stránka, žádná změna

                return true
            }
            R.id.editUser -> {
                // Otevření stránky s nastavením
                val newActivity = Intent(this, EditUserActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.logout -> {
                // Vymazání údajů o přihlášeném uživately
                LoginActivity.UserData.userID = 0
                LoginActivity.UserData.role = ""

                //Navrácení na přihlášení
                val newActivity = Intent(this, LoginActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            else -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }
}

// end EditPilotsActivity.kt
