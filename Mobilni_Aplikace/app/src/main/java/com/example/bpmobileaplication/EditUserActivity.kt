package com.example.bpmobileaplication

/*
    EditUserActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita pro editaci účtu uživatele
*/

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.database.getStringOrNull
import com.example.bpmobileaplication.LoginActivity.UserData.role
import com.example.bpmobileaplication.LoginActivity.UserData.userID
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import java.io.IOException

class EditUserActivity : AppCompatActivity() {

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Načtení údajů o přihlášeném uživately z databáze
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        // Odstranění symbolů spinneru které už jsou používány
        var notUsedSymbols = resources.getStringArray(R.array.characters).toList()
        var cursor = db.rawQuery("""SELECT * FROM User WHERE  Role='pilot' AND ID != $userID""", null)
        if(cursor.moveToFirst()){
            do{
                notUsedSymbols = notUsedSymbols.filterIndexed{i, _ -> i !== notUsedSymbols.indexOf(cursor.getString(cursor.getColumnIndex("Symbol")))}
            } while (cursor.moveToNext())
        }

        cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$userID""", null)
        cursor.moveToFirst()

        // Rozděleni layoutu podle typu osoby
        if(role == "owner"){
            setContentView(R.layout.edit_user_owner_layout)
        }
        else {
            setContentView(R.layout.edit_user_normal_layout)

            var symbol = cursor.getString(cursor.getColumnIndex("Symbol"))

            // Načtení dat do spinneru
            val characterSpinner = findViewById<Spinner>(R.id.symbol)
            val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, notUsedSymbols)
            spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
            characterSpinner.adapter = spinnerAdapter

            // nastavení počáteční hodnoty spinneru
            characterSpinner.setSelection(notUsedSymbols.indexOf(symbol))
        }

        // Nastavení výchozích hodnot do ostatních prvků
        // Heslo ne, bylo by to divný
        var name = cursor.getString(cursor.getColumnIndex("Name"))

        val nameEditText = findViewById<EditText>(R.id.name)
        nameEditText.setText(name)

        db.close()

        // nastavení lisenerů
        val saveButton = findViewById<Button>(R.id.save_button)
        saveButton.setOnClickListener{ saveData() }
    }

    // Kontrola dat a uložení do databáze
    private fun saveData() {
        val nameEditText = findViewById<EditText>(R.id.name)
        val passwordEditText = findViewById<EditText>(R.id.password)

        val name = nameEditText.text.toString()
        val password = passwordEditText.text.toString()
        var symbol = ""

        // Kontrola dat
        // Jméno a symbol musí být unikátní
        if (name == ""){
            Toast.makeText(this, "Jméno nesmí být prázdné", Toast.LENGTH_SHORT).show()
            return
        }

        var dbHelper = FeedReaderDbHelper(this)
        var db = dbHelper.readableDatabase

        // Získání aktuálních údajů o uživately z databáze
        var cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$userID""", null)
        cursor.moveToFirst()

        var actualName = cursor.getString(cursor.getColumnIndex("Name"))
        var actualSymbol = cursor.getStringOrNull(cursor.getColumnIndex("Symbol")) // Pro majitele není nastavený znak

        // Vyjímka na aktuální jméno aby se nehlásilo že je už zabrané
        if(name != actualName){
            cursor = db.rawQuery("""SELECT * FROM User WHERE Name='$name'""", null)
            if (cursor.count != 0){
                Toast.makeText(this, "Jméno je již používáno", Toast.LENGTH_SHORT).show()
                db.close()
                return
            }
        }

        if(role != "owner") {
            val symbolSpinner = findViewById<Spinner>(R.id.symbol)
            symbol = symbolSpinner.selectedItem.toString()

            // Vyjímka na aktuální symbol aby se nehlásilo že je už zabraný
            if(symbol != actualSymbol){
                cursor = db.rawQuery("""SELECT * FROM User WHERE Symbol='$symbol'""", null)
                if (cursor.count != 0) {
                    Toast.makeText(this, "Symbol je už používaný", Toast.LENGTH_SHORT).show()
                    db.close()
                    return
                }
            }
        }

        db.close()

        // Upravení hodnot na zařízení
        var message = if(role == "owner"){
            if(password == ""){
                "Edit User ID:$userID~ Values Name:$name~"
            } else{
                "Edit User ID:$userID~ Values Name:$name~Password:$password~"
            }
        }
        else{
            if(password == ""){
                "Edit User ID:$userID~ Values Name:$name~Symbol:$symbol~"
            } else{
                "Edit User ID:$userID~ Values Name:$name~Password:$password~Symbol:$symbol~"
            }
        }

        try {
            m_bluetoothSocket!!.outputStream.write(message.toByteArray())

            val buffer = ByteArray(1024)
            var bytes = m_bluetoothSocket!!.inputStream.read(buffer)
            var responce = String(buffer, 0, bytes)

            if (responce != "OK"){
                Toast.makeText(this, "Změna údajů se nepodařila", Toast.LENGTH_SHORT).show()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "Spojení přerušeno, záznam nelze upravovat", Toast.LENGTH_SHORT).show()
            return
        }

        // Upravení hodnot ve vnitřní databázi
        dbHelper = FeedReaderDbHelper(this)
        db = dbHelper.writableDatabase

        if (role == "owner"){
            if(password == ""){
                db.execSQL("""UPDATE User SET Name='$name' WHERE ID=$userID""")
            }
            else{
                db.execSQL("""UPDATE User SET Name='$name', Password='$password' WHERE ID=$userID""")
            }
        }
        else{
            if(password == ""){
                db.execSQL("""UPDATE User SET Name='$name', Symbol='$symbol' WHERE ID=$userID""")
            }
            else{
                db.execSQL("""UPDATE User SET Name='$name', Password='$password', Symbol='$symbol' WHERE ID=$userID""")
            }
        }

        db.close()

        Toast.makeText(this, "Nové údaje uloženy", Toast.LENGTH_SHORT).show()
    }

    // Zobrazení menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(role == "owner"){
            menuInflater.inflate(R.menu.menu_owner, menu)
        }
        else {
            menuInflater.inflate(R.menu.menu_normal, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    // Akce pro jedlotlivé prvky v menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.records -> {
                //Navrácení na přehled letů
                val newActivity = Intent(this,  FlightsActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.editPilots -> {
                // Otevření stránky pro editaci uživatelů
                val newActivity = Intent(this, EditPilotsActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.editUser -> {
                // Aktuální stránka, žádná změna
                return true
            }
            R.id.logout -> {
                // Vymazání údajů o přihlášeném uživately
                userID = 0
                role = ""

                //Navrácení na přihlášení
                val newActivity = Intent(this,  LoginActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            else -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }
}

// end EditUserActivity.kt
