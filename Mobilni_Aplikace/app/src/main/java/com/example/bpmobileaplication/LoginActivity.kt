package com.example.bpmobileaplication

/*
    LoginActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita umožňující příhlášení uživatele
*/

import android.app.ProgressDialog
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import java.io.File
import java.io.IOException
import java.util.*

class LoginActivity: AppCompatActivity() {

    // Objekt pro indikaci zda jsou data ze zařízen uložena ve vnitřní databázi
    companion object{
        var dataLoaded = false
    }

    // Objekt pro uchování dat o aktuálně přihlášeném uživately
    object UserData {
        var userID = 0
        var role = ""
    }

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.login_layout)

        // Nastavení liseneru na stisknutí tlačítka login
        val refreshButton = findViewById<Button>(R.id.login)
        refreshButton.setOnClickListener{ login() }

        // Nastavení liseneru na stisknutí tlačítka disconect
        val disconnectButton = findViewById<Button>(R.id.disconnect)
        disconnectButton.setOnClickListener{ disconnect() }

        // Načtení dat jen když se jedná o nové připojení
        val dbFile: File = getDatabasePath("AirplaneData.db")
        if(!dbFile.exists()){
            LoadData(this).execute()
        }
    }

    // Kontrola přihlášení s databází na zařízení
    private fun login()
    {
        // Ověření údajů přes Bluetooth
        if (dataLoaded){
            val nameEditText = findViewById<EditText>(R.id.name)
            val passwordEditText = findViewById<EditText>(R.id.password)
            val name = nameEditText.text.toString()
            val password = passwordEditText.text.toString()

            // Kontrola dat z databází
            val dbHelper = FeedReaderDbHelper(this)
            val db = dbHelper.readableDatabase

            var userFound = false

            // Test jestli se uživatel nachází v databázi
            val cursor = db.rawQuery("""SELECT * FROM User WHERE Name='$name' AND Password='$password'""", null)
            if(cursor.moveToFirst()){
                userFound = true

                UserData.userID = cursor.getInt(cursor.getColumnIndex("ID"))
                UserData.role = cursor.getString(cursor.getColumnIndex("Role"))
            }

            db.close()

            // Pokud je jméno a heslo platné, tak se přejde na další okno
            if (userFound){
                // Otevření hlavního okna po přihlášení
                val newActivity = Intent(this, FlightsActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)
            }
            else{
                Toast.makeText(this, "Neplatný uživatel", Toast.LENGTH_LONG).show()
            }
        }
    }

    // Odpojení od zařízení
    fun disconnect() {
        // Uzavření bluetooth spojení
        if (m_bluetoothSocket != null) {
            try {
                m_bluetoothSocket!!.close()
                m_bluetoothSocket = null
                SelectDeviceActivity.BlueToothConnection.m_isConnected = false
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        // Otevření aktivity víběru zařízení
        val newActivity = Intent(this, SelectDeviceActivity::class.java)
        newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        finish()
        startActivity(newActivity)
    }

    // Třída pro načtení databáze ze zařízení
    private class LoadData(c: AppCompatActivity) : AsyncTask<Void, Void, String>() {
        private var loadingSuccess: Boolean = false
        private val context: AppCompatActivity
        lateinit var m_progress: ProgressDialog

        init {
            this.context = c
        }

        // Zapnatí načítacího dialogu před zahájením načítání dat
        override fun onPreExecute() {
            super.onPreExecute()

            m_progress = ProgressDialog.show(context, "Načítání dat ze zařízení...", "čekejte prosím")
        }

        // Přenesení dat ze zařízení do mobilu
        override fun doInBackground(vararg p0: Void?): String? {
            if(m_bluetoothSocket != null){
                try {
                    var buffer = ByteArray(1024)
                    var bytes: Int
                    var message = ""
                    var responce = ""

                    val dbHelper = FeedReaderDbHelper(context)
                    val db = dbHelper.writableDatabase

                    message = "Send table User"
                    m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                    bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                    responce = String(buffer, 0, bytes)

                    // Kontrola jestli je odpověď počet záznamů
                    if(responce.toIntOrNull() == null){
                        return null
                    }

                    // Čtení dat z tabulky User
                    // Uložení všech záznamů
                    for(i in 1..responce.toInt()){
                        message = "Send"
                        m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                        bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                        responce = String(buffer, 0, bytes)

                        // Získání hodnot z odpovědi
                        val regexID = Regex("""ID:([^~]*)~""")
                        val regexName = Regex("""Name:([^~]*)~""")
                        val regexPassword = Regex("""Password:([^~]*)~""")
                        val regexRole = Regex("""Role:([^~]*)~""")
                        val regexSymbol = Regex("""Symbol:([^~]*)~""")

                        Log.i("regexID", "" + responce)

                        var ID = regexID.find(responce)?.groups?.get(1)?.value!!
                        var Name = regexName.find(responce)?.groups?.get(1)?.value!!
                        var Password = regexPassword.find(responce)?.groups?.get(1)?.value!!
                        var Role = regexRole.find(responce)?.groups?.get(1)?.value!!
                        var Symbol = regexSymbol.find(responce)?.groups?.get(1)?.value!!


                        // Vložení hodnot do DB
                        // Vyřešení prázdného znaku u majitele
                        if(Symbol == "None"){
                            db.execSQL("""INSERT INTO User (ID, Name, Password, Role, Symbol) VALUES ($ID, '$Name', '$Password', '$Role', null);""")
                        }else{
                            db.execSQL("""INSERT INTO User (ID, Name, Password, Role, Symbol) VALUES ($ID, '$Name', '$Password', '$Role', '$Symbol');""")
                        }
                    }

                    message = "Send table Flight"
                    m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                    bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                    responce = String(buffer, 0, bytes)

                    // Kontrola jestli je odpověď počet záznamů
                    if(responce.toIntOrNull() == null){
                        return null
                    }

                    // Čtení dat z tabulky Flight
                    for(i in 1..responce.toInt()){
                        message = "Send"
                        m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                        bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                        responce = String(buffer, 0, bytes)

                        Log.i("regexID", "" + responce)

                        // Získání hodnot z odpovědi
                        val regexID = Regex("""ID:([^~]*)~""")
                        val regexPilot_ID = Regex("""Pilot_ID:([^~]*)~""")

                        var ID = regexID.find(responce)?.groups?.get(1)?.value!!
                        var Pilot_ID = regexPilot_ID.find(responce)?.groups?.get(1)?.value!!

                        // Vložení hodnot do DB
                        // Pokud let nemá pilota uložit null
                        if (Pilot_ID == "None"){
                            db.execSQL("""INSERT INTO Flight (ID, Pilot_ID) VALUES ($ID, null);""")
                        }else{
                            db.execSQL("""INSERT INTO Flight (ID, Pilot_ID) VALUES ($ID, $Pilot_ID);""")
                        }
                    }

                    message = "Send table GPS_Log"
                    m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                    bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                    responce = String(buffer, 0, bytes)

                    // Kontrola jestli je odpověď počet záznamů
                    if(responce.toIntOrNull() == null){
                        return null
                    }

                    // Čtení dat z tabulky GPS_Log
                    for(i in 1..responce.toInt()){
                        message = "Send"
                        m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                        bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                        responce = String(buffer, 0, bytes)

                        Log.i("regexID", "" + responce)

                        // Získání hodnot z odpovědi
                        val regexID = Regex("""ID:([^~]*)~""")
                        val regexFlight_ID = Regex("""Flight_ID:([^~]*)~""")
                        val regexLatitude = Regex("""Latitude:([^~]*)~""")
                        val regexLongitude = Regex("""Longitude:([^~]*)~""")
                        val regexTimestamp = Regex("""Timestamp:([^~]*)~""")

                        var ID = regexID.find(responce)?.groups?.get(1)?.value!!
                        var Flight_ID = regexFlight_ID.find(responce)?.groups?.get(1)?.value!!
                        var Latitude = regexLatitude.find(responce)?.groups?.get(1)?.value!!
                        var Longitude = regexLongitude.find(responce)?.groups?.get(1)?.value!!
                        var Timestamp = regexTimestamp.find(responce)?.groups?.get(1)?.value!!

                        // Vložení hodnot do DB
                        // Vyřešení nulových hodnot u GPS
                        if(Latitude == "None" && Longitude == "None"){
                            db.execSQL("""INSERT INTO GPS_Log (ID, Flight_ID, Latitude, Longitude, Timestamp) VALUES ($ID, $Flight_ID, null, null, '$Timestamp');""")
                        }
                        else if(Latitude == "None"){
                            db.execSQL("""INSERT INTO GPS_Log (ID, Flight_ID, Latitude, Longitude, Timestamp) VALUES ($ID, $Flight_ID, null, '$Longitude', '$Timestamp');""")
                        }
                        else if (Longitude == "None"){
                            db.execSQL("""INSERT INTO GPS_Log (ID, Flight_ID, Latitude, Longitude, Timestamp) VALUES ($ID, $Flight_ID, '$Latitude', null, '$Timestamp');""")
                        }
                        else {
                            db.execSQL("""INSERT INTO GPS_Log (ID, Flight_ID, Latitude, Longitude, Timestamp) VALUES ($ID, $Flight_ID, '$Latitude', '$Longitude', '$Timestamp');""")
                        }

                    }

                    db.close()
                    loadingSuccess = true
                } catch (e: IOException) { //IOException
                    e.printStackTrace()
                }
            }
            return null
        }

        // Vypnutí načítacího dialogu a vrácení výsledku navázání spojení
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            m_progress.dismiss()
            if (!loadingSuccess) {
                Log.i("data", "Couldn't load")
                Toast.makeText(context, "Chyba náčítání dat", Toast.LENGTH_SHORT).show()
            } else {
                dataLoaded = true
                Toast.makeText(context, "Data načtena", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

// end LoginActivity.kt
