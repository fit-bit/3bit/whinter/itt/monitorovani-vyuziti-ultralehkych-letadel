package com.example.bpmobileaplication

/*
    SelectDeviceActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita umožňující navázaní spojení s napárovaným Bluetooth zařízením
*/

import android.app.Activity
import android.app.ProgressDialog
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.Context
import android.content.Intent
import android.os.AsyncTask
import android.os.Bundle
import android.util.Log
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_address
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothAdapter
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_isConnected
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_myUUID
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_progress
import java.io.IOException
import java.util.*
import kotlin.collections.ArrayList

class SelectDeviceActivity : AppCompatActivity()
{
    // Objekt uchovávající bluetooth spojení se zařízením
    object BlueToothConnection {
        var m_myUUID: UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
        var m_bluetoothSocket: BluetoothSocket? = null
        lateinit var m_progress: ProgressDialog
        lateinit var m_bluetoothAdapter: BluetoothAdapter
        var m_isConnected: Boolean = false
        lateinit var m_address: String
    }

    private var m_bluetoothAdapter: BluetoothAdapter? = null
    private lateinit var m_pairedDevices: Set<BluetoothDevice>
    private val REQUEST_ENABLE_BLUETOOTH = 1

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.select_device_layout)

        // Získání bluetooth, když null tak na zařízení není blue tooth
        m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
        if(m_bluetoothAdapter == null) {
            Toast.makeText(this, "Zařízení neobsahuje Bluetooth", Toast.LENGTH_LONG).show()
            return
        }

        // Povolení zapnutí bluetooth
        if(!m_bluetoothAdapter!!.isEnabled) {
            val enableBluetoothIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            startActivityForResult(enableBluetoothIntent, REQUEST_ENABLE_BLUETOOTH)
        }

        // Nastavení liseneru na stisknutí tlačítka refresh
        val refreshButton = findViewById<Button>(R.id.select_device_refresh)
        refreshButton.setOnClickListener{ pairedDeviceList() }
    }

    // Vypsání zpárovaných zařízení do listu
    private fun pairedDeviceList() {
        m_pairedDevices = m_bluetoothAdapter!!.bondedDevices
        val listBluetooth : ArrayList<BluetoothDevice> = ArrayList()
        val listNames: ArrayList<String> = ArrayList()

        // Získání zařízení s názvy
        if (m_pairedDevices.isNotEmpty()) {
            for (device: BluetoothDevice in m_pairedDevices) {
                listBluetooth.add(device)
                listNames.add(device.name + " : " + device)
                Log.i("device", "" + device)
            }
        } else {
            Toast.makeText(this, "Nenalezeny žádné spárované zařízení", Toast.LENGTH_LONG).show()
        }

        // Přepsání pole spárovaných zažízení do seznamu
        val adapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, listNames)

        // Nastavení liseneru na stisk na položku seznamu
        val device_list = findViewById<ListView>(R.id.select_device_list)
        device_list.adapter = adapter
        device_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            val device: BluetoothDevice = listBluetooth[position]
            m_address = device.address

            // Smazání databáze když existuje
            this.deleteDatabase("AirplaneData.db");

            // Připojení k zařízení
            ConnectToDevice(this).execute()
        }
    }

    // Výsledek povolení Bluetooth
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == REQUEST_ENABLE_BLUETOOTH) {
            if (resultCode == Activity.RESULT_OK) {
                if (m_bluetoothAdapter!!.isEnabled) {
                    Toast.makeText(this, "Bluetooth povoleno", Toast.LENGTH_LONG).show()
                } else {
                    Toast.makeText(this, "Bluetooth zakázáno", Toast.LENGTH_LONG).show()
                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(this, "Povolení Bluetooth bylo zakázáno", Toast.LENGTH_LONG).show()
            }
        }
    }

    // Třída pro navázání bluetooth spojení
    private class ConnectToDevice(c: AppCompatActivity) : AsyncTask<Void, Void, String>() {
        private var connectSuccess: Boolean = true
        private val context: AppCompatActivity

        init {
            this.context = c
        }

        // Zapnatí načítacího dialogu před zahájením spojování
        override fun onPreExecute() {
            super.onPreExecute()
            m_progress = ProgressDialog.show(context, "Připojování...", "čekejte prosím")
        }

        // Pokus o navázání spojení
        override fun doInBackground(vararg p0: Void?): String? {
            try {
                if (m_bluetoothSocket == null || !m_isConnected) {
                    m_bluetoothAdapter = BluetoothAdapter.getDefaultAdapter()
                    val device: BluetoothDevice = m_bluetoothAdapter.getRemoteDevice(m_address)
                    m_bluetoothSocket = device.createInsecureRfcommSocketToServiceRecord(m_myUUID)
                    BluetoothAdapter.getDefaultAdapter().cancelDiscovery()
                    m_bluetoothSocket!!.connect()
                }
            } catch (e: IOException) {
                connectSuccess = false
                e.printStackTrace()
            }
            return null
        }

        // Vypnutí načítacího dialogu a vrácení výsledku navázání spojení
        override fun onPostExecute(result: String?) {
            super.onPostExecute(result)

            m_progress.dismiss()
            if (!connectSuccess) {
                Log.i("data", "Couldn't connect")
                Toast.makeText(context, "Chyba spojení", Toast.LENGTH_SHORT).show()
            } else {
                m_isConnected = true

                val newActivity = Intent(context, LoginActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                context.finish()
                context.startActivity(newActivity)
            }
        }
    }
}

// end SelectDeviceActivity.kt
