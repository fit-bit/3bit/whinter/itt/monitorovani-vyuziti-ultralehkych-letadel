package com.example.bpmobileaplication

/*
    FlightDetailActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita zobrazující detail letu
*/

import android.app.DownloadManager
import android.content.pm.PackageManager
import android.graphics.Color
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.bpmobileaplication.LoginActivity.UserData.role
import com.example.bpmobileaplication.LoginActivity.UserData.userID
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import com.google.android.gms.maps.*
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit

class FlightDetailActivity : AppCompatActivity(), OnMapReadyCallback {

    // ID aktuálně prohlíženého letu
    var flight_ID = 0

    // Request kód pro povolení zápisu do úložiště
    val WRITE_RQ = 101

    // Mapa
    private lateinit var mMap: GoogleMap
    private val MAP_VIEW_BUNDLE_KEY = "MapViewBundleKey"

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)

        // Záskání ID letu
        flight_ID = intent.getIntExtra("FLIGHT_ID", 0)

        // Rozhodnutí který layout zobrazit
        var gpsLogsComplmete = true

        // Projití logů a zjištění jestli existují všechny gps souřadnice
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        var cursor = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flight_ID""", null)
        if(cursor.moveToFirst()){
            do{
                var latitude = cursor.getInt(cursor.getColumnIndex("Latitude"))
                var longitude = cursor.getString(cursor.getColumnIndex("Longitude"))

                if(latitude == null || longitude == null){
                    gpsLogsComplmete = false
                }

            } while (cursor.moveToNext())
        }

        if(gpsLogsComplmete){
            setContentView(R.layout.flight_detail_layout)

            // Získání mapy a její načtení
            val mapFragment = supportFragmentManager.findFragmentById(R.id.mapView) as SupportMapFragment
            mapFragment.getMapAsync(this)
        }else{
            setContentView(R.layout.flight_detail_no_map_layout)
        }

        // Načtení dat o letu
        cursor = db.rawQuery(
            """SELECT * FROM Flight INNER JOIN User ON User.ID = Flight.Pilot_ID AND Flight.ID=$flight_ID""",
            null
        )

        // Kontrola jestli let má pilota
        var pilotName = "..."
        if (cursor.count != 0){
            cursor.moveToFirst()
            pilotName = cursor.getString(cursor.getColumnIndex("Name"))
        }

        // Informace z GPS
        // První hodnota
        cursor = db.rawQuery(
            """SELECT * FROM GPS_Log WHERE Flight_ID=$flight_ID ORDER BY Timestamp ASC""",
            null
        )
        cursor.moveToFirst()

        // Předělání datetime do čitelnějšího formátu pro výpis na display
        val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")

        var startDatetime = formatter.format(parser.parse(cursor.getString(cursor.getColumnIndex("Timestamp"))))
        val startLatitude = cursor.getString(cursor.getColumnIndex("Latitude"))
        val startLongitude = cursor.getString(cursor.getColumnIndex("Longitude"))

        // Poslední hodnota
        cursor = db.rawQuery(
            """SELECT * FROM GPS_Log WHERE Flight_ID=$flight_ID ORDER BY Timestamp ASC""",
            null
        )
        cursor.moveToLast()
        var endDatetime = formatter.format(parser.parse(cursor.getString(cursor.getColumnIndex("Timestamp"))))
        val endLatitude = cursor.getString(cursor.getColumnIndex("Latitude"))
        val endLongitude = cursor.getString(cursor.getColumnIndex("Longitude"))

        // Čas letu
        val flightTimeMilliseconds = (formatter.parse(endDatetime).time - formatter.parse(
            startDatetime
        ).time)
        val flightTime = java.lang.String.format(
            "%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(
                flightTimeMilliseconds
            ),
            TimeUnit.MILLISECONDS.toMinutes(flightTimeMilliseconds) - TimeUnit.HOURS.toMinutes(
                TimeUnit.MILLISECONDS.toHours(
                    flightTimeMilliseconds
                )
            ),
            TimeUnit.MILLISECONDS.toSeconds(flightTimeMilliseconds) - TimeUnit.MINUTES.toSeconds(
                TimeUnit.MILLISECONDS.toMinutes(
                    flightTimeMilliseconds
                )
            )
        )

        // Zobrazení do layoutu
        val startLatitudeView = findViewById<TextView>(R.id.start_latitude)
        val startLongitudeView = findViewById<TextView>(R.id.start_longitude)
        val startDatetimeView = findViewById<TextView>(R.id.start_datetime)

        val endLatitudeView = findViewById<TextView>(R.id.end_latitude)
        val endLongitudeView = findViewById<TextView>(R.id.end_longitude)
        val endDatetimeView = findViewById<TextView>(R.id.end_datetime)

        val flightTimeView = findViewById<TextView>(R.id.flight_time)
        val pilotView = findViewById<TextView>(R.id.pilot)

        startLatitudeView.text = startLatitude
        startLongitudeView.text = startLongitude
        startDatetimeView.text = startDatetime

        endLatitudeView.text = endLatitude
        endLongitudeView.text = endLongitude
        endDatetimeView.text = endDatetime

        flightTimeView.text = flightTime
        pilotView.text = pilotName

        // Naplnění spineru pro přidání/změnu pilota u letu
        // Načtení dat která se mají ve spinneru objevit
        // Majitel může změnit kohokoli, pilot může přidat jen sebe
        var pilots = ArrayList<String>()
        if(role == "owner"){
            var cursor = db.rawQuery("""SELECT * FROM User""", null)
            if(cursor.moveToFirst()){
                do{
                    val userName = cursor.getString(cursor.getColumnIndex("Name"))

                    // Vynechání vlastníka, nemá lety
                    if(cursor.getInt(cursor.getColumnIndex("ID")) != userID){
                        pilots.add(userName)
                    }
                } while (cursor.moveToNext())
            }
        }
        else{
            var cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$userID""", null)
            cursor.moveToFirst()

            val userName = cursor.getString(cursor.getColumnIndex("Name"))
            pilots.add(userName)
        }

        db.close()

        val pilotSpinner = findViewById<Spinner>(R.id.spinner_pilot)
        val spinnerAdapter = ArrayAdapter(this, android.R.layout.simple_list_item_1, pilots)
        spinnerAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
        pilotSpinner.adapter = spinnerAdapter

        // nastavení počáteční hodnoty spinneru pokud let má pilota
        if(pilotName != "..."){
            pilotSpinner.setSelection(pilots.indexOf(pilotName))
        }

        // Nastavení lisenerů
        val saveButton = findViewById<Button>(R.id.save_gps)
        saveButton.setOnClickListener{ checkSavePermissions() }

        val editPilotButton = findViewById<Button>(R.id.button_edit_pilot)
        editPilotButton.setOnClickListener{ editPilot() }
    }

    // Poždádání o oprávnění zapsat data pokud ho nemáme
    private fun checkSavePermissions(){
        // Požádání o oprávnění uložit soubor pokud toto právo nemáme
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(
                    applicationContext,
                    android.Manifest.permission.WRITE_EXTERNAL_STORAGE
                ) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(
                    this,
                    arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE),
                    WRITE_RQ
                )
                return
            }
        }

        // Povolení je, soubor se může uložit
        saveData()
    }

    // Zjištění návratu dialogu zda uživatel povolil přístup k datům mobilu
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
            // Povolení zamítnuto nelze uložit
            Toast.makeText(this, "Povolení neuděleno, data nelze uložit", Toast.LENGTH_SHORT).show()
        } else{
            // Povolení získáné, soubor se může uložit
            saveData()
        }
    }

    // Změna pilota u letu
    private fun editPilot(){
        // Získání vybrané hodnoty ze spineru
        val pilotSpinner = findViewById<Spinner>(R.id.spinner_pilot)
        val pilotName = pilotSpinner.selectedItem.toString()

        // Změna hodnoty na zařízení
        val message = "Change Pilot Flight_ID:$flight_ID~Pilot_Name:$pilotName~"
        try {
            m_bluetoothSocket!!.outputStream.write(message.toByteArray())

            val buffer = ByteArray(1024)
            var bytes = m_bluetoothSocket!!.inputStream.read(buffer)
            var responce = String(buffer, 0, bytes)

            if (responce != "OK"){
                Toast.makeText(this, "Změna pilota se nezdařila", Toast.LENGTH_SHORT).show()
            }
        } catch (e: IOException) {
            e.printStackTrace()
            Toast.makeText(this, "Spojení přerušeno, záznam nelze upravovat", Toast.LENGTH_SHORT).show()
            return
        }

        // Upravení hodnot ve vnitřní databázi
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.writableDatabase

        db.execSQL("""UPDATE Flight SET Pilot_ID=(SELECT ID FROM User WHERE Name='$pilotName') WHERE ID=$flight_ID""")

        db.close()

        // Změna pilota v seznamu
        val pilotView = findViewById<TextView>(R.id.pilot)
        pilotView.text = pilotName

        Toast.makeText(this, "Pilot změněn", Toast.LENGTH_SHORT).show()
    }

    // Uložení GPS záznamů o konkrétním letu
    private fun saveData(){
        // Zapsání souboru s gps logy do Download složky
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        val filename = "GPSLog_$flight_ID.csv"
        var fileData = ""

        // Připravení dat csv formátu
        // Hlavička
        fileData += "Timestamp,Latitude,Longitude\n"

        // Projití všech GPS logů ke konkrétnímu letu v databázi
        val cursor = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flight_ID""", null)
        if(cursor.moveToFirst()){
            do{
                val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")

                var timestamp = formatter.format(
                    parser.parse(
                        cursor.getString(
                            cursor.getColumnIndex(
                                "Timestamp"
                            )
                        )
                    )
                )
                val latitude = cursor.getString(cursor.getColumnIndex("Latitude"))
                val longitude = cursor.getString(cursor.getColumnIndex("Longitude"))

                fileData += "$timestamp,$latitude,$longitude\n"
            } while (cursor.moveToNext())
        }
        db.close()

        // Odstranění posledného prvku v řetězci \n
        fileData = fileData.dropLast(1)

        try {
            // Vytvoření souboru
            val file = File(
                Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                filename
            )

            // Naplnění souboru daty
            var fos = FileOutputStream(file)
            fos.write(fileData.toByteArray())
            fos.close()

            // Oznámení uživately že se soubor stáhl
            // Přístup k souboru možný přes složku se staženými soubory
            val downloadManager =   this.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            downloadManager.addCompletedDownload(
                file.getName(),
                file.getName(),
                true,
                "text/csv",
                file.getAbsolutePath(),
                file.length(),
                true
            )
        } catch (e: Exception) {
            e.printStackTrace()

            Toast.makeText(this, "Chyba ukládání", Toast.LENGTH_SHORT).show()
        }

        Toast.makeText(this, "Záznamy uloženy do složky se staženýmy soubory", Toast.LENGTH_SHORT).show()
    }

    // Zobrazení letu do mapy
    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        // Načtení všech GPS dat z databáze
        var GPSDataList = ArrayList<LatLng>()

        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        val cursor = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flight_ID ORDER BY Timestamp ASC""", null)
        if(cursor.moveToFirst()){
            do{
                val latitude = cursor.getString(cursor.getColumnIndex("Latitude"))
                val longitude = cursor.getString(cursor.getColumnIndex("Longitude"))

                GPSDataList.add(LatLng(latitude.toFloat().toDouble(),
                    longitude.toFloat().toDouble()
                ))
            } while (cursor.moveToNext())
        }

        db.close()

        // Přidání značky na začátek a konec cesty
        mMap.addMarker(MarkerOptions().position(GPSDataList.first()).title("Start"))
        mMap.addMarker(MarkerOptions().position(GPSDataList.last()).title("End"))

        // Přidání cesty do mapy
        mMap.addPolyline(
            PolylineOptions().addAll(GPSDataList)
                .width(8f)
                .color(Color.RED)
        )

        // Zazoomování na start
        mMap.animateCamera(
            CameraUpdateFactory.newLatLngZoom(
                GPSDataList.first(),
                10f
            )
        )

    }
}

// end FlightDetailActivity.kt
