package com.example.bpmobileaplication

/*
    FlightsActivity.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Aktivita zobrazující seznam letů uložených na vestavěném zařízení
*/

import android.app.DownloadManager
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.example.bpmobileaplication.FlightsActivity.Flights.flightsList
import com.example.bpmobileaplication.LoginActivity.UserData.role
import com.example.bpmobileaplication.LoginActivity.UserData.userID
import com.example.bpmobileaplication.SelectDeviceActivity.BlueToothConnection.m_bluetoothSocket
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.concurrent.TimeUnit


class FlightsActivity : AppCompatActivity() {

    // Výchozí hodnoty pro řazení
    var sortDatetimeAscending = true
    var sortPilotAscending = true

    // Request kód pro povolení zápisu do úložiště
    val WRITE_RQ = 101

    // Objekt uchovávající načtené lety pro přihlášeného uživatele
    object Flights{
        lateinit var flightsList : List<Flight>
    }

    // Zobrazení stránky
    override fun onCreate(savedInstanceState: Bundle?)
    {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.flights_layout)

        // Nastavení lisenerů
        val saveButton = findViewById<Button>(R.id.save_button)
        saveButton.setOnClickListener{ checkSavePermissions() }

        val datetimeText = findViewById<TextView>(R.id.datetime_label)
        datetimeText.setOnClickListener{ sortDatetime() }

        val pilotText = findViewById<TextView>(R.id.pilot_label)
        pilotText.setOnClickListener{ sortPilot() }

        loadList()
    }

    // Přenačtení dat v listu, mohl se změnit pilot
    override fun onRestart() {
        super.onRestart()

        // Měna tak by se zachovalo řazení
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        for(i in 0 until flightsList.count()){
            val flightID = flightsList[i].ID

            val cursor = db.rawQuery("""SELECT * FROM User INNER JOIN Flight ON User.ID = Flight.Pilot_ID AND Flight.ID=$flightID""", null)

            // Přeskoření letů bez pilota
            if(cursor.moveToFirst()){
                val pilotInDB  = cursor.getString(cursor.getColumnIndex("Name"))

                // Pokud se hodnota v databází nezhoduje s hodnotou v listu -> změna v listu
                if(pilotInDB != flightsList[i].pilot){
                    flightsList[i].pilot = pilotInDB

                    // změna textu
                    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")
                    val readableDatetime: String = formatter.format(parser.parse(flightsList[i].datetime))

                    flightsList[i].text = "$readableDatetime | $pilotInDB"
                }
            }
        }

        db.close()

        // Přenačtení listu
        createList()
    }

    // Načtení letů pro zobrazení přihlášenému uživately
    // Vlastník vydí všechny lety, Pilot jen svoje a lety bez pilota
    private fun loadList(){
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        var flights = ArrayList<Flight>()

        if(role == "owner") {
            val cursor = db.rawQuery("""SELECT * FROM User INNER JOIN Flight ON User.ID = Flight.Pilot_ID""", null)
            if(cursor.moveToFirst()){
                do{
                    var flightID = cursor.getInt(cursor.getColumnIndex("Flight.ID"))
                    var pilot = cursor.getString(cursor.getColumnIndex("Name"))

                    // Získání prvníního času z GPS logů pro zobrazení jako času uskutečnení letu
                    val cursorInner = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flightID ORDER BY Timestamp ASC""", null)
                    cursorInner.moveToFirst()
                    var datetime = cursorInner.getString(cursorInner.getColumnIndex("Timestamp"))

                    // Předělání datetime do čitelnějšího formátu pro výpis na display
                    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")
                    val readableDatetime: String = formatter.format(parser.parse(datetime))

                    var text = "$readableDatetime | $pilot"
                    flights.add(Flight(flightID, pilot, datetime, text))
                } while (cursor.moveToNext())
            }
        }
        else {
            val cursor = db.rawQuery("""SELECT * FROM User INNER JOIN Flight ON User.ID = Flight.Pilot_ID AND Flight.Pilot_ID=$userID """, null)
            if(cursor.moveToFirst()){
                do{
                    var flightID = cursor.getInt(cursor.getColumnIndex("Flight.ID"))
                    var pilot = cursor.getString(cursor.getColumnIndex("Name"))

                    // Získání prvníního času z GPS logů pro zobrazení jako času uskutečnení letu
                    val cursorInner = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flightID ORDER BY Timestamp ASC""", null)
                    cursorInner.moveToFirst()
                    var datetime = cursorInner.getString(cursorInner.getColumnIndex("Timestamp"))

                    // Předělání datetime do čitelnějšího formátu pro výpis na display
                    val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                    val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")
                    val readableDatetime: String = formatter.format(parser.parse(datetime))

                    var text = "$readableDatetime | $pilot"
                    flights.add(Flight(flightID, pilot, datetime, text))
                } while (cursor.moveToNext())
            }
        }

        // Přidání letů bez pilota
        val cursor = db.rawQuery("""SELECT * FROM Flight WHERE Pilot_ID is null""", null)
        if(cursor.moveToFirst()){
            do{
                var flightID = cursor.getInt(cursor.getColumnIndex("Flight.ID"))
                var pilot = "..."

                // Získání prvníního času z GPS logů pro zobrazení jako času uskutečnení letu
                val cursorInner = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flightID ORDER BY Timestamp ASC""", null)
                cursorInner.moveToFirst()
                var datetime = cursorInner.getString(cursorInner.getColumnIndex("Timestamp"))

                // Předělání datetime do čitelnějšího formátu pro výpis na display
                val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
                val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")
                val readableDatetime: String = formatter.format(parser.parse(datetime))

                var text = "$readableDatetime | $pilot"
                flights.add(Flight(flightID, pilot, datetime, text))
            } while (cursor.moveToNext())
        }

        // Uložení do objektu pro uchování
        // ArrayList nefungoval jako globální proměnná
        flightsList = flights

        db.close()

        // Zobrazení načtených hodnot do seznamu pro zobrazení
        createList()
    }

    // Zobrazení menu
    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        if(role == "owner"){
            menuInflater.inflate(R.menu.menu_owner, menu)
        }
        else {
            menuInflater.inflate(R.menu.menu_normal, menu)
        }
        return super.onCreateOptionsMenu(menu)
    }

    // Akce pro jedlotlivé prvky v menu
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item.itemId){
            R.id.records -> {
                // Aktuální stránka, žádná změna
                return true
            }
            R.id.editPilots -> {
                // Otevření stránky pro editaci uživatelů
                val newActivity = Intent(this, EditPilotsActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.editUser -> {
                // Otevření stránky s nastavením
                val newActivity = Intent(this, EditUserActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            R.id.logout -> {
                // Vymazání údajů o přihlášeném uživately
                userID = 0
                role = ""

                //Navrácení na přihlášení
                val newActivity = Intent(this, LoginActivity::class.java)
                newActivity.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
                finish()
                startActivity(newActivity)

                return true
            }
            else -> {

            }
        }
        return super.onOptionsItemSelected(item)
    }

    // Přepsání načtených hodnot do listu pro zobrazení uživately
    private fun createList(){
        val adapter = ArrayAdapter(this, R.layout.custom_item_layout, R.id.textItem, flightsList.map { it.text }) //flightsList.map { it.text }

        // Nastavení lisenerů na stisk na položku seznamu
        val flight_list = findViewById<ListView>(R.id.flights_list)
        flight_list.adapter = adapter
        flight_list.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
            // Zobrazení konkrétního letu
            val newActivity = Intent(this, FlightDetailActivity::class.java)
            newActivity.putExtra("FLIGHT_ID", flightsList[position].ID) // Vybraný let
            startActivity(newActivity)
        }

        flight_list.onItemLongClickListener =  AdapterView.OnItemLongClickListener { _, _, position, _ ->
            // Zobrazení okna na smazání letu
            // Jen pro majitele
            if (role == "owner"){
                val builder = AlertDialog.Builder(this)
                builder.setMessage("Opravdu chete tento záznam smazat?")
                        .setCancelable(false)
                        .setPositiveButton("Ano") { dialog, id ->
                            // Smazání záznamu z databáze a zobrazení listu se smazaným prvkem
                            var flightID = flightsList[position].ID


                            // Smazání letu ze zařízení
                            var message = "Delete Flight ID:$flightID"

                            // Kontrola jestli se zařízení neodpojilo
                            try {
                                m_bluetoothSocket!!.outputStream.write(message.toByteArray())

                                val buffer = ByteArray(1024)
                                var bytes = m_bluetoothSocket!!.inputStream.read(buffer)
                                var responce = String(buffer, 0, bytes)

                                if (responce != "OK"){
                                    Toast.makeText(this, "Záznam se nepodařilo smazat", Toast.LENGTH_SHORT).show()
                                }
                            } catch (e: IOException) {
                                e.printStackTrace()
                                Toast.makeText(this, "Spojení přerušeno, záznam nelze smazat", Toast.LENGTH_SHORT).show()

                                return@setPositiveButton
                            }

                            // Smazání letu z lokální databáze
                            val dbHelper = FeedReaderDbHelper(this)
                            val db = dbHelper.writableDatabase

                            db.execSQL("""PRAGMA foreign_keys = ON""")
                            db.execSQL("""DELETE FROM Flight WHERE ID=$flightID""")
                            db.close()

                            // Složitý způsob pro vymazání konkrétného prvku, protože z listu nejde mazat
                            var flights = ArrayList<Flight>()
                            for ((i, item) in flightsList.withIndex()){
                                if (i != position){
                                    flights.add(item)
                                }
                            }

                            flightsList = flights
                            createList()
                        }
                        .setNegativeButton("Ne") { dialog, id ->
                            dialog.dismiss()
                        }
                val alert = builder.create()
                alert.show()
            }

            true
        }
    }

    // Poždádání o oprávnění zapsat data pokud ho nemáme
    private fun checkSavePermissions(){

        // Požádání o oprávnění uložit soubor pokud toto právo nemáme
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            if (ContextCompat.checkSelfPermission(applicationContext, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED){
                ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE), WRITE_RQ)
                return
            }
        }

        // Povolení je, soubor se může uložit
        saveData()
    }

    // Zjištění návratu dialogu zda uživatel povolil přístup k datům mobilu
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
       if (grantResults.isEmpty() || grantResults[0] != PackageManager.PERMISSION_GRANTED){
           // Povolení zamítnuto nelze uložit
           Toast.makeText(this, "Povolení neuděleno, data nelze uložit", Toast.LENGTH_SHORT).show()
       } else{
            // Povolení získáné, soubor se může uložit
           saveData()
        }
    }

    // Uložení zobrazených letů do excel formátu
    private fun saveData(){
        // Zapsání souboru s lety do Download složky
        // Získání jména aktuálního uživatele pro název uložení
        val dbHelper = FeedReaderDbHelper(this)
        val db = dbHelper.readableDatabase

        val cursor = db.rawQuery("""SELECT * FROM User WHERE ID=$userID""", null)
        cursor.moveToFirst()
        var userName = cursor.getString(cursor.getColumnIndex("Name"))

        val filename = "filghts_$userName.csv"
        var fileData = ""

        // Připravení dat csv formátu
        // Hlavička
        fileData += "Flight ID,Pilot Name,Flight Time,Start Datetime,Start Latitude,Start Longitude,End Datetime,End Latitude,End Longitude\n"

        // Projití všech letů které jsou uloženy v listu a sehnání k nim podrobných dat z databáze
        for (flight in flightsList){

            // Informace o letu
            val flightID = flight.ID
            var cursor = db.rawQuery("""SELECT * FROM Flight INNER JOIN User ON User.ID = Flight.Pilot_ID AND Flight.ID=$flightID""", null)

            // Kontrola jestli let má pilota
            var pilotName = "..."
            if (cursor.count != 0){
                cursor.moveToFirst()
                pilotName = cursor.getString(cursor.getColumnIndex("Name"))
            }

            // Informace z GPS
            // První hodnota
            cursor = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flightID ORDER BY Timestamp ASC""", null)
            cursor.moveToFirst()

            // Předělání datetime do čitelnějšího formátu pro výpis na display
            val parser = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            val formatter = SimpleDateFormat("HH:mm:ss dd.MM.yyyy")

            var startDatetime = formatter.format(parser.parse(cursor.getString(cursor.getColumnIndex("Timestamp"))))
            val startLatitude = cursor.getString(cursor.getColumnIndex("Latitude"))
            val startLongitude = cursor.getString(cursor.getColumnIndex("Longitude"))

            // Poslední hodnota
            cursor = db.rawQuery("""SELECT * FROM GPS_Log WHERE Flight_ID=$flightID ORDER BY Timestamp ASC""", null)
            cursor.moveToLast()
            var endDatetime = formatter.format(parser.parse(cursor.getString(cursor.getColumnIndex("Timestamp"))))
            val endLatitude = cursor.getString(cursor.getColumnIndex("Latitude"))
            val endLongitude = cursor.getString(cursor.getColumnIndex("Longitude"))

            // Čas letu
            val flightTimeMilliseconds = (formatter.parse(endDatetime).time - formatter.parse(startDatetime).time)
            val flightTime = java.lang.String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(flightTimeMilliseconds),
                    TimeUnit.MILLISECONDS.toMinutes(flightTimeMilliseconds) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(flightTimeMilliseconds)),
                    TimeUnit.MILLISECONDS.toSeconds(flightTimeMilliseconds) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(flightTimeMilliseconds)))

            // Uložení do řetězce
            fileData += "$flightID,$pilotName,$flightTime,$startDatetime,$startLatitude,$startLongitude,$endDatetime,$endLatitude,$endLongitude\n"
        }
        db.close()

        // Odstranění posledného prvku v řetězci \n
        fileData = fileData.dropLast(1)

        try {
            // Vytvoření souboru
            val file = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename)

            // Naplnění souboru daty
            var fos = FileOutputStream(file)
            fos.write(fileData.toByteArray())
            fos.close()

            // Oznámení uživately že se soubor stáhl
            // Přístup k souboru možný přes složku se staženými soubory
            val downloadManager =   this.getSystemService(DOWNLOAD_SERVICE) as DownloadManager
            downloadManager.addCompletedDownload(file.getName(), file.getName(), true, "text/csv", file.getAbsolutePath(), file.length(), true)
        } catch (e: Exception) {
            e.printStackTrace()

            Toast.makeText(this, "Chyba ukládání", Toast.LENGTH_SHORT).show()
        }

        Toast.makeText(this, "Záznamy uloženy do složky se staženýmy soubory", Toast.LENGTH_SHORT).show()
    }

    // Řazení seznamu podle času a data
    private fun sortDatetime(){
        val dateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        if(sortDatetimeAscending){
            flightsList = flightsList.sortedWith(compareBy { dateFormat.parse(it.datetime) })
            sortDatetimeAscending = false
        }
        else {
            flightsList = flightsList.sortedWith(compareByDescending { dateFormat.parse(it.datetime) })
            sortDatetimeAscending = true
        }

        // Update listu podle nově seřazených dat
        createList()
    }

    // Řazení listu podle pilota
    private fun sortPilot(){
        if(sortPilotAscending){
            flightsList = flightsList.sortedWith(compareBy { it.pilot })
            sortPilotAscending = false
        }
        else {
            flightsList = flightsList.sortedWith(compareByDescending { it.pilot })
            sortPilotAscending = true
        }

        // Update listu podle nově seřazených dat
        createList()
    }
}

// Třída pro uchování potřebných hodnot o konkrétním letu
class Flight(val ID: Int, var pilot: String, val datetime: String, var text: String) {
}

// end FlightsActivity.kt
