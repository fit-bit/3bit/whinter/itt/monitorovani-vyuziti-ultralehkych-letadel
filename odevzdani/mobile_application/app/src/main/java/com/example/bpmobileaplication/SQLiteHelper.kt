package com.example.bpmobileaplication

/*
    SQLiteHelper.kt
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Třída zastřešující databázi ve které jsou uložena načtená data z vestavěného zařízení
*/

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper

class FeedReaderDbHelper(context: Context) : SQLiteOpenHelper(context, DATABASE_NAME, null, DATABASE_VERSION) {

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL("""CREATE TABLE IF NOT EXISTS User ( ID INTEGER PRIMARY KEY AUTOINCREMENT, Name text NOT NULL, Password text NOT NULL, Role text NOT NULL, Symbol VARCHAR(1));""")
        db.execSQL(""" CREATE TABLE IF NOT EXISTS Flight ( ID INTEGER PRIMARY KEY AUTOINCREMENT, Pilot_ID INTEGER, CONSTRAINT fk_pilot_name FOREIGN KEY (Pilot_ID) REFERENCES User (ID) ON DELETE CASCADE ON UPDATE CASCADE); """)
        db.execSQL(""" CREATE TABLE IF NOT EXISTS GPS_Log ( ID INTEGER PRIMARY KEY AUTOINCREMENT, Flight_ID INTEGER NOT NULL, Latitude DECIMAL(15,10), Longitude DECIMAL(15,10), Timestamp DATETIME NOT NULL, CONSTRAINT fk_flight_id FOREIGN KEY (Flight_ID) REFERENCES Flight (ID) ON DELETE CASCADE ON UPDATE CASCADE); """)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        // Upgrade smaže všechna data a vytvoří prázdnou databázi
        db.execSQL(""" DROP TABLE IF EXISTS User; """)
        db.execSQL(""" DROP TABLE IF EXISTS Flight; """)
        db.execSQL(""" DROP TABLE IF EXISTS GPS_Log; """)

        onCreate(db)
    }

    override fun onDowngrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        onUpgrade(db, oldVersion, newVersion)
    }

    companion object {
        // Při změně schématu databáze je potřeba zvětšit verzi databáze
        const val DATABASE_VERSION = 1
        const val DATABASE_NAME = "AirplaneData.db"
    }
}

// end SQLiteHelper.kt
