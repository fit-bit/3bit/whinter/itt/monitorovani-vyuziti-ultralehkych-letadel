Postup naistalování systému na Raspberry Pi.

1. Nainstalování operačního systému Raspbian

2. Updatování
sudo apt update
sudo apt full-upgrade

3 Povolení Serial portu a I2C v nastavení zařízení

sudo raspi-config

4. Instalace potřebných rozšiřujících knihoven

sudo python3 -m pip install pybluez
sudo apt-get install bluetooth

5. Bluetooth servis se v základu spustí se špatným nastavením a je potřeba v souboru /lib/systemd/system/bluetooth.service změnit 

ExecStart=/usr/lib/bluetooth/bluetoothd
na
ExecStart=/usr/lib/bluetooth/bluetoothd -C

6. Poté je potřeba přenačíst deamon a povolit seriovou komunikaci přes Bluetooth

sudo systemctl daemon-reload
sudo systemctl restart bluetooth
sudo sdptool add SP

7. Vytvořte složku do které budou umístěny soubory systému, lokace je volitelná

8. Do složky je následně potřeba přesunout soubory: SDL_DS3231.py,  bluetooth_process.py, database_init.py, flight_recorder_process.py

9. V souborech bluetooth_process.py, database_init.py, flight_recorder_process.py, je potřeba změnit cestu na novou složku co byla vytvořena pro soubory systému. Parametr RUN_FOLDER_PATH 

10. Ve vytvořené složce je potřeba inicializovat databázi, v mobilní aplikaci bude výchozí uživazel majitel letadla se jménem: admin a heslem: admin

sudo python3 database_init.py

11. Dále je potřeba překopírovat Service soubory do složky /etc/systemd/system. Potřebné je však ještě změnit cestu k spouštěnému souboru podle složky kerá byla pro soubory vytvořena

12. Po překopírování servisů je potřeba restartovat deamon pro zprávu servisů aby je zaznamenal a je potřeba servisi povolit aby se automaticky spustily při dalším startu zařízení

sudo systemctl daemon-reload
sudo systemctl enable bluetooth_aplication.service
sudo systemctl enable flight_recorder_process.service

13. Pro následné používání stačí zařízení restartovat a pak je už možné se s zařízením spárovat a přihlasit se jako admin přes kterého lze nastavit všechny uživatele

##############################################################################
Pro bezdrátovou úpravu parametů vestavěného zařízení jde použít SSH nebo Virtual desktop

1. Pro sprovoznění SSH je potřeba ho povolit v 

sudo raspi-config

2. Pro povolení Virtual desktop je potřeba provést příkazy

sudo apt-get install xrdp
sudo apt-get install tightvncserver
