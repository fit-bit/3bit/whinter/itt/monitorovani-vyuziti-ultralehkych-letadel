#!/usr/bin/env python3
"""
    bluetooth_process.py
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Proces ridici vymenu dat mezi mobilni aplikaci a databazi s ulozenimy daty
"""

import bluetooth
import subprocess
import sqlite3
import re

RUN_FOLDER_PATH = "/home/pi/Desktop/airplane-monitoring/"
DATABASE_NAME = 'airplane_data.db'

# Napojeni databaze
conn = sqlite3.connect(RUN_FOLDER_PATH+DATABASE_NAME)
databaze = conn.cursor()

# Nastaveni bluetooth
subprocess.check_output("sudo bluetoothctl power on", shell = True)
subprocess.check_output("sudo bluetoothctl discoverable on", shell = True)
subprocess.check_output("sudo bluetoothctl pairable on", shell = True)
subprocess.check_output("sudo bluetoothctl agent NoInputNoOutput", shell = True)

subprocess.check_output("sudo hciconfig hci0 sspmode 1", shell = True)

# Nastaveni pro bluetooth rozhrani
port = 1
uuid = "94f39d29-7d6d-437d-973b-fba39e49d4ee"

# Nekonecna smycka cekajici na spojeni
try:
    while(True):
        server_sock = bluetooth.BluetoothSocket(bluetooth.RFCOMM)
        server_sock.bind(("", 1))
        server_sock.listen(1)

        bluetooth.advertise_service(server_sock, "AirplaneServer", service_id=uuid,
                                    service_classes=[uuid, bluetooth.SERIAL_PORT_CLASS],
                                    profiles=[bluetooth.SERIAL_PORT_PROFILE],
                                    protocols=[bluetooth.OBEX_UUID]
                                    )

        print("Waiting for connection on RFCOMM channel", port)

        client_sock, client_info = server_sock.accept()
        print("Accepted connection from", client_info)

        # Nekonecna smycka prijimani pozadavku
        try:
            while True:
                data = client_sock.recv(1024).decode('ascii')
                if not data:
                    break

                # Identifikace pozadavku a vybrani odpovedi
                print("Received: ", repr(data))

                if (re.match(r'^Send table User$', data)):
                    # Ziskani vsech zaznamu z tabulky a postupne poslani
                    databaze.execute("SELECT * FROM User")
                    result = databaze.fetchall()

                    client_sock.send(str(len(result)))
                    print("Send: ", repr(str(len(result))))

                    for line in result:
                        data = client_sock.recv(1024).decode('ascii') # Prijeti spravy jen pro synchronizaci odesilani
                        if not data:
                            break

                        print("Received: ", repr(data))

                        send_message = "ID:" + str(line[0]) + "~Name:" + line[1] + "~Password:" + line[2] + "~Role:" + line[3] + "~Symbol:" + str(line[4]) + "~"
                        client_sock.send(send_message)
                        print("Send: ", repr(send_message))

                elif (re.match(r'^Send table Flight$', data)):
                    # Ziskani vsech zaznamu z tabulky a postupne poslani
                    databaze.execute("SELECT * FROM Flight")
                    result = databaze.fetchall()

                    client_sock.send(str(len(result)))
                    print("Send: ", repr(str(len(result))))

                    for line in result:
                        data = client_sock.recv(1024).decode('ascii') # Prijeti spravy jen pro synchronizaci odesilani
                        if not data:
                            break

                        print("Received: ", repr(data))

                        send_message = "ID:" + str(line[0]) + "~Pilot_ID:" + str(line[1]) + "~"
                        client_sock.send(send_message)
                        print("Send: ", repr(send_message))

                elif (re.match(r'^Send table GPS_Log$', data)):
                    # Ziskani vsech zaznamu z tabulky a postupne poslani
                    databaze.execute("SELECT * FROM GPS_Log")
                    result = databaze.fetchall()


                    client_sock.send(str(len(result))) # Poslani kolik zaznamu se prenese
                    print("Send: ", repr(str(len(result))))

                    for line in result:
                        data = client_sock.recv(1024).decode('ascii') # Prijeti spravy jen pro synchronizaci odesilani
                        if not data:
                            break

                        print("Received: ", repr(data))

                        send_message = "ID:" + str(line[0]) + "~Flight_ID:" + str(line[1]) + "~Latitude:" + str(line[2]) + "~Longitude:" + str(line[3]) + "~Timestamp:" + line[4] + "~"
                        client_sock.send(send_message)
                        print("Send: ", repr(send_message))

                elif (re.match(r'^Delete Flight ID:.*$', data)):
                    # Smazani letu
                    flight_id = re.search(r'ID:(.*)', data).group(1)
                    databaze.execute("""PRAGMA foreign_keys = ON """) # Smaze i hodnoty napojene pres cizi klice
                    databaze.execute("DELETE FROM Flight WHERE ID=?",(flight_id,))
                    conn.commit() # ulozeni zmen

                    send_message = "OK"
                    client_sock.send(send_message)
                    print("Send: ", repr(send_message))

                elif (re.match(r'^Change Pilot Flight_ID:.*~Pilot_Name:.*~', data)):
                    # Zmena pilota u letu
                    flight_id = re.search(r'Flight_ID:([^~]*)~', data).group(1)
                    pilot_name = re.search(r'Pilot_Name:([^~]*)~', data).group(1)

                    databaze.execute("UPDATE Flight SET Pilot_ID=(SELECT ID FROM User WHERE Name=?) WHERE ID=?", (pilot_name, flight_id,))
                    conn.commit() # ulozeni zmen

                    send_message = "OK"
                    client_sock.send(send_message)
                    print("Send: ", repr(send_message))

                elif (re.match(r'^Delete User ID:.*$', data)):
                    # Smazani uzivatele
                    user_id = re.search(r'ID:(.*)', data).group(1)
                    databaze.execute("""PRAGMA foreign_keys = ON """) # Smaze i hodnoty napojene pres cizi klice
                    databaze.execute("DELETE FROM User WHERE ID=?",(user_id,))
                    conn.commit() # ulozeni zmen

                    send_message = "OK"
                    client_sock.send(send_message)
                    print("Send: ", repr(send_message))

                elif (re.match(r'^Create User Values .*$', data)):
                    # Vytvoreni uzivatele
                    name = re.search(r'Name:([^~]*)~', data).group(1)
                    password = re.search(r'Password:([^~]*)~', data).group(1)
                    symbol = re.search(r'Symbol:([^~]*)~', data).group(1)

                    databaze.execute("INSERT INTO User (Name, Password, Role, Symbol) VALUES (?, ?, 'pilot', ?)", (name, password, symbol,))
                    conn.commit() # ulozeni zmen

                    send_message = "OK"
                    client_sock.send(send_message)
                    print("Send: ", repr(send_message))

                elif (re.match(r'^Edit User ID:.* Values .*$', data)):
                    # Uprava udaju u uzivatele
                    code = "UPDATE User SET "

                    # Ziskani dat k uprave
                    if(re.search(r'Name:([^~]*)~', data)):
                        name = re.search(r'Name:([^~]*)~', data).group(1)
                        code += "Name='%s', " % name

                    if(re.search(r'Password:([^~]*)~', data)):
                        password = re.search(r'Password:([^~]*)~', data).group(1)
                        code += "Password='%s', " % password

                    if(re.search(r'Symbol:([^~]*)~', data)):
                        symbol = re.search(r'Symbol:([^~]*)~', data).group(1)
                        code += "Symbol='%s', " % symbol

                    # odstraneni carky za posledni hodnotou
                    code = code[:-2]

                    user_id = re.search(r'ID:([^~]*)~', data).group(1)
                    code += " WHERE ID=%s" % user_id

                    databaze.execute(code)
                    conn.commit() # ulozeni zmen

                    send_message = "OK"
                    client_sock.send(send_message)
                    print("Send: ", repr(send_message))
                else:
                    client_sock.send("Invalid action")
        except OSError:
            pass

        print("Disconnected.")

        client_sock.close()
        server_sock.close()

finally:
    conn.close()

# end bluetooth_process.py