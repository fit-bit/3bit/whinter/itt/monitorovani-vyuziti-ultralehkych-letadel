"""
    database_init.py
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)
    
    Inicializace databáze využívé programy vestavěného zařízení
"""

import sqlite3

RUN_FOLDER_PATH = "/home/pi/Desktop/airplane-monitoring/"
DATABASE_NAME = 'airplane_data.db'

# Pokud databáze neexistuje, tak se vytvoří ve stejné složce jako ostatní programy systému
conn = sqlite3.connect(RUN_FOLDER_PATH+DATABASE_NAME)
c = conn.cursor()

# Smazani tabulek kdyz existuji
c.execute(""" DROP TABLE IF EXISTS User; """)
c.execute(""" DROP TABLE IF EXISTS Flight; """)
c.execute(""" DROP TABLE IF EXISTS GPS_Log; """)

##### Vytvoření tabulek #####

# Uživatelé
c.execute(""" CREATE TABLE IF NOT EXISTS User (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                Name text NOT NULL,
                Password text NOT NULL,
                Role text NOT NULL,
                Symbol VARCHAR(1)
            ); """)

# Let
c.execute(""" CREATE TABLE IF NOT EXISTS Flight (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                Pilot_ID INTEGER,
                CONSTRAINT fk_pilot_name
                FOREIGN KEY (Pilot_ID) REFERENCES User (ID)
                ON DELETE CASCADE
                ON UPDATE CASCADE
            ); """)

# Trasa letu
c.execute(""" CREATE TABLE IF NOT EXISTS GPS_Log (
                ID INTEGER PRIMARY KEY AUTOINCREMENT,
                Flight_ID INTEGER NOT NULL,
                Latitude DECIMAL(15,10),
                Longitude DECIMAL(15,10),
                Timestamp DATETIME NOT NULL,
                CONSTRAINT fk_flight_id
                FOREIGN KEY (Flight_ID) REFERENCES Flight (ID)
                ON DELETE CASCADE
                ON UPDATE CASCADE
            ); """)

        
# Vlozeni vychoziho uzivatele
c.execute(""" INSERT INTO User (Name, Password, Role) VALUES ('admin', 'admin', 'owner'); """)

c.execute("""PRAGMA foreign_keys = ON """)

conn.commit()

# end database_init.py
