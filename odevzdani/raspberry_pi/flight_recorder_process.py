#!/usr/bin/env python3
"""
    flight_recorder_process.py
    Řešení bakalářské práce: Monitorování využití ultralehkých letadel
    Autor: Matěj Kudera, FIT (xkuder04@stud.fit.vutbr.cz)

    Proces ridici periferie a zaznam letu
"""

import RPi.GPIO as GPIO
import serial
import re
import datetime
import os.path
import sqlite3
import SDL_DS3231

"""
Zapojeni

Tlacitko:
12 - Logicka 1
16 - Vystup ztisku

Display: Common anode
37 - VCC
7 - G
11 - F
13 - A
15 - B
29 - E
31 - D
33 - C
35 - DP

RTC
17 - VCC 3.3V
9 - GND
3 - SDA
5 - SCL

GPS
4 - VCC 5V
6 - GND
8 TXD - RXD
10 RXD - TXD

Pocitadlo otacek motoru
40 - vstup inpulzu
1 - dioda
39 - ultralight GND
"""

########## Konstanty ###################
RUN_FOLDER_PATH = "/home/pi/Desktop/airplane-monitoring/"
DATABASE_NAME = 'airplane_data.db'

START_DECISION_SPEED_KNOTS = 20
STOP_DECISION_SPEED_KNOTS = 15
START_DELAY_SECONDS = 10
STOP_DELAY_SECONDS = 10

LOGS_DELAY_SECONDS = 10
LOG_FILE_NAME = 'flight_log.txt'

EVALUATE_PULSES_AFTER_SECONDS = 1
AVERAGING_ARRAY_LENGHT = 10

FLIGHT_START_REVOLUTIONS = 4000

########## Pomocne promenne ###########

# Spojeni s databazi a ziskani zabranych identifikatoru pilotu
used_pilot_symbols = []

conn = sqlite3.connect(RUN_FOLDER_PATH+DATABASE_NAME)
database = conn.cursor()

database.execute("SELECT * FROM User")
result = database.fetchall()

for line in result:
    if line[4] != None:
        used_pilot_symbols.append(line[4])

# Promenna pro uchovani aktualne vybraneho pilota
actual_pilot = ''
actual_pilot_list_index = ""

# Piny segmentu displaye
segments = (7,11,13,15,29,31,33,35)

# Nataveni pinu pro jednotlive cislice
num = {'':(0,0,0,0,0,0,0,0),
    ' ':(1,1,1,1,1,1,1,1),
    '0':(1,0,0,0,0,0,0,1),
    '1':(1,1,1,0,1,1,0,1),
    '2':(0,1,0,0,0,0,1,1),
    '3':(0,1,0,0,1,0,0,1),
    '4':(0,0,1,0,1,1,0,1),
    '5':(0,0,0,1,1,0,0,1),
    '6':(0,0,0,1,0,0,0,1),
    '7':(1,1,0,0,1,1,0,1),
    '8':(0,0,0,0,0,0,0,1),
    '9':(0,0,0,0,1,1,0,1),
    'A':(0,0,0,0,0,1,0,1),
    'C':(1,0,0,1,0,0,1,1),
    'E':(0,0,0,1,0,0,1,1),
    'F':(0,0,0,1,0,1,1,1),
    'G':(1,0,0,1,0,0,0,1),
    'H':(0,0,1,0,0,1,0,1),
    'J':(1,1,1,0,1,0,0,1),
    'L':(1,0,1,1,0,0,1,1),
    'P':(0,0,0,0,0,1,1,1),
    'U':(1,0,1,0,0,0,0,1)}

# Pomocne promenne urcene pro urceni stavu ledadla
decision_datetime = None
in_flight = False

# Soubor pro uchovani souboru s logi a casove razitko pro stanoveni casu mezi jednotlivimi logi
last_log_datetime = datetime.datetime.now()

# Ziskana GPS data
data_chunk = ""

# Promene zajistujici blikani pokud neni vybrany pilot
flash_state = True
flash_delay = 1
flash_datetime = datetime.datetime.now()

# Casove razitko ky byly naposled vyhodnoceny pulzy z otackomeru
last_datetime_of_pulses_evaluation = datetime.datetime.now()

# Promena do ktere se zaznamenavaji pulzi od civky zapalovani motoru
pulses_counter = 0

# Promena pro uchovani aktualnich otacek motoru
motor_resolutions = 0

########## Pomocne funkce ###########

# Callback na zmenu pilota kdyz se zmackne tlacitko
def button_callback(channel):
    global RUN_FOLDER_PATH
    global LOG_FILE_NAME
    global actual_pilot
    global used_pilot_symbols
    global actual_pilot_list_index

    if actual_pilot == '':
        actual_pilot = used_pilot_symbols[0]
        actual_pilot_list_index = 0
    else:
        actual_pilot_list_index = (actual_pilot_list_index + 1) % len(used_pilot_symbols)
        actual_pilot = used_pilot_symbols[actual_pilot_list_index]

    # Ulozeni selekce do log souboru
    make_log("Pilot:"+actual_pilot+"\n", RUN_FOLDER_PATH+LOG_FILE_NAME)

    for loop in range(0,8):
        GPIO.output(segments[loop], num[str(actual_pilot)][loop])

# Vyparsovani potrebnich dat z GPS NMEA formatu
def parse_GPS(data_chunk):
    # Vsechny potrebne data jsou v $GPRMC
    UTC_TIME_FIX = ""
    DATE_STAMP = ""
    LATITUDE = ""
    LONGITUDE = ""
    SPEED_OVER_GROUND_IN_KNOTS = ""

    # Ziskani GPRMC casti
    gprmc_string = ""
    gprmc_match = re.search(r"(\$GPRMC.*)\r\n", data_chunk)

    if (gprmc_match != None):
        gprmc_string = gprmc_match.group(1)
    else:
        print("No match")
        return (UTC_TIME_FIX, DATE_STAMP,LATITUDE, LONGITUDE, SPEED_OVER_GROUND_IN_KNOTS)

    # Kontrola kontrolniho souctu
    checksum_split = gprmc_string.split("*")
    data = checksum_split[0]
    recieved_checksum = checksum_split[1]

    # $ se do checksum nepocita
    data = data[1:]
    checksum = 0
    for char in data:
        if checksum == 0:
            checksum = ord(char)
        else:
            checksum = checksum ^ ord(char)

    checksum_hex_string = str(hex(checksum))
    checksum_hex_string = checksum_hex_string[2:].upper()

    if (checksum_hex_string != recieved_checksum):
        print("Checksum error, recieved data corupted")
        return (UTC_TIME_FIX, DATE_STAMP,LATITUDE, LONGITUDE, SPEED_OVER_GROUND_IN_KNOTS)

    # Narezani GPRMC na jednotlive casti
    gprmc_split = gprmc_string.split(",")

    # Kontrola validity GPRMC dat
    if (gprmc_split[2] == 'V'):
        print("Reciever warning, no fix")
        return (UTC_TIME_FIX, DATE_STAMP,LATITUDE, LONGITUDE, SPEED_OVER_GROUND_IN_KNOTS)

    # Ziskani potrebnych dat
    UTC_TIME_FIX = gprmc_split[1]
    DATE_STAMP = gprmc_split[9]
    LATITUDE = gprmc_split[3]
    LONGITUDE = gprmc_split[5]
    SPEED_OVER_GROUND_IN_KNOTS = gprmc_split[7]

    return (UTC_TIME_FIX, DATE_STAMP,LATITUDE, LONGITUDE, SPEED_OVER_GROUND_IN_KNOTS)

# Zapsani logu do souboru
def make_log(message, log_file_name):
    log_file = open(log_file_name, "a")
    log_file.write(message)
    log_file.close()

    return True

# Ulozeni zaznamenanich logu do database
def save_logfile_to_database(log_file_name):
    global database
    log_file = open(log_file_name, "r")

    pilot_character = None
    logs = ""
    for line in log_file:
        # Vybrani posledniho zaznamu o vyberu pilota
        pilot_match = re.search(r"^Pilot:(.)", line)
        if (pilot_match != None):
            pilot_character = pilot_match.group(1)

        # Ulozeni letoveho log zaznamu
        log_match = re.search(r"^(.*),(.*):(.*),(.*)$", line)
        if (log_match != None):
            logs = logs + line

    # Let nebyl uskutecnen pokud v log souboru nejsou zadne GPS logi
    if logs == "":
        log_file.close()
        os.remove(log_file_name)
        return True

    # Ulozeni letu do DB
    if pilot_character == None:
        database.execute(""" INSERT INTO Flight (Pilot_ID) VALUES (null); """)
    else:
        # Pokud by se za letu zmenil symbol pilot v databazi, tak se let ulozi s neznamim pilotem
        try:
            database.execute(""" INSERT INTO Flight (Pilot_ID) SELECT ID FROM User WHERE Symbol = (?); """, (pilot_character))
        except:
            database.execute(""" INSERT INTO Flight (Pilot_ID) VALUES (null); """)

    conn.commit() # ulozeni zmen
    new_flight_id = database.lastrowid

    for line in logs.splitlines():
        gps_log = re.search(r"^(.*),(.*):(.*),(.*)$", line)

        # Rozdeleni na zaznamy z GPS a bez
        latitude_DD = None
        longitude_DD = None
        if (gps_log.group(3) != "" and gps_log.group(4) != ""):
            # GPS dava data ve formatu DMS (degrees, minutes, seconds), pro vyuziti v mobilni aplikaci vsak musi byt predelana do formatu DD (decimal degrees)
            latitude_degrees = float(gps_log.group(3)[:2])
            latitude_minutes = float(gps_log.group(3)[2:])
            latitude_DD = latitude_degrees + latitude_minutes/60

            longitude_degrees = float(gps_log.group(4)[1:3])
            longitude_minutes = float(gps_log.group(4)[3:])
            longitude_DD = longitude_degrees + longitude_minutes/60

        log_datetime = datetime.datetime.strptime(gps_log.group(1)+","+gps_log.group(2), "%m/%d/%Y,%H:%M:%S")

        database.execute(" INSERT INTO GPS_Log (Flight_ID, Latitude, Longitude, Timestamp) VALUES (?,?,?,?); ", (new_flight_id, latitude_DD, longitude_DD, log_datetime.strftime("%Y-%m-%dT%H:%M:%S.%f")[:-3]))
        conn.commit() # ulozeni zmen

    log_file.close()
    os.remove(log_file_name)

    return True

# Pricitac impulzu ktere na pin prisly
def add_impulse_callback(channel):
    global pulses_counter

    pulses_counter = pulses_counter + 1

########## Inicializace periferii #########

GPIO.setwarnings(False)
GPIO.setmode(GPIO.BOARD)

# Nastaveni pinu tlacitka
GPIO.setup(12, GPIO.OUT)
GPIO.output(12, 1)
GPIO.setup(16, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(16, GPIO.RISING, callback=button_callback, bouncetime=300) #pro zabraneni vice interuptu se 200ms ignoruji dalsi interupty

# Inicializace pinu segmentoveho displaye
for segment in segments:
    GPIO.setup(segment, GPIO.OUT)
    GPIO.output(segment, 0)

# Napajeci pin pro segmentovy display
GPIO.setup(37, GPIO.OUT)
GPIO.output(37, 1)

# Nastaveni pinu a callbacku pro pocitani pulzu
GPIO.setup(40, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
GPIO.add_event_detect(40, GPIO.RISING, callback=add_impulse_callback) # U hran by nemely byt zachvevy

# Nastaveni serialu pro GPS
serialPort = serial.Serial("/dev/ttyS0", 9600, timeout=0.5)
serialPort.flushInput()

# Nastavei spojeni s RTC modulem
rtc_modul = SDL_DS3231.SDL_DS3231(1, 0x68)

########### Inicilazace logovaciho souboru ##############
# Ulozeni starsich logu pokud se neulzili pred poslednim vypnutim systemu
if os.path.isfile(RUN_FOLDER_PATH+LOG_FILE_NAME):
    if os.path.getsize(RUN_FOLDER_PATH+LOG_FILE_NAME) > 0:
        save_logfile_to_database(RUN_FOLDER_PATH+LOG_FILE_NAME)
    else:
        os.remove(RUN_FOLDER_PATH+LOG_FILE_NAME)

########## Nekonecna smycka procesu ##########
try:
    while True:
        # Prevod na otacky za minutu, zapalovaci civka dava 2 pulzy na otacku
        # Datetime kompenzuje dobu trvani cyklu, vetsinou kolem 1s
        motor_resolutions = pulses_counter*((datetime.datetime.now() - last_datetime_of_pulses_evaluation).total_seconds())*60/2

        pulses_counter = 0
        last_datetime_of_pulses_evaluation = datetime.datetime.now()

        # Nacteni bloku GPS dat
        # Blok je ukoncen GPGLL zaznamem
        data_chunk = ""

        # Cteni jednoho bloku trva 1s
        while True:
            line = serialPort.readline()

            # Nekdy dorazi divny znak, preskocit
            try:
                line = line.decode('utf-8')
            except:
                continue

            data_chunk = data_chunk + line

            # Kontrola konce aktualniho bloku dat
            if re.search(r"^(\$GPGLL.*)\r\n", line) != None:
                break

        # Pokud neni vybrany zadny pilot tak bude display blikat
        if actual_pilot == '':
            if round((datetime.datetime.now() - flash_datetime).total_seconds()) >= flash_delay:
                if flash_state:
                    for loop in range(0,8):
                        GPIO.output(segments[loop], num[str(actual_pilot)][loop])
                    flash_state = False
                else:
                    for loop in range(0,8):
                        GPIO.output(segments[loop], num[' '][loop])
                    flash_state = True

                flash_datetime = datetime.datetime.now()
        else:
            # Jinak zobrazit aktualniho pilota
            for loop in range(0,8):
                GPIO.output(segments[loop], num[str(actual_pilot)][loop])

        # Zpracovani ziskanich GPS dat
        gps_data = parse_GPS(data_chunk)

        utc_time = gps_data[0]
        date_stamp = gps_data[1]
        latitude = gps_data[2]
        longitude = gps_data[3]
        knots = gps_data[4]

        # Konvertovani dat do vyuzitelneho formatu
        if knots == "":
            knots = 0
        else:
            knots = float(knots)

        print("Knots: %s"%(knots))
        print("Revolutions: %s"%(motor_resolutions))

        # Vyhodnoceni GPS dat a otacek motoru. Urceni aktualniho stavu systemu
        if in_flight:
            if decision_datetime == None:
                # Pokud je ve fazi aktivniho letu nulova rychlost tak nejsou dostupna GPS data, let ukoncen vypnutim motoru
                if (knots == 0):
                    # Ledadlo je ve stavu letu, vytvoreni zaznamu kazdych x sekund
                    if (datetime.datetime.now() - last_log_datetime).total_seconds() > LOGS_DELAY_SECONDS:
                        # Zapsani logu bez GPS
                        make_log(rtc_modul.read_datetime().strftime("%m/%d/%Y,%H:%M:%S")+":,\n", RUN_FOLDER_PATH+LOG_FILE_NAME)
                        last_log_datetime = datetime.datetime.now()

                    print("Airplane in flight, only motor data")

                elif (knots >= STOP_DECISION_SPEED_KNOTS):
                    # Ledadlo je ve stavu letu, vytvoreni zaznamu kazdych x sekund
                    if (datetime.datetime.now() - last_log_datetime).total_seconds() > LOGS_DELAY_SECONDS:
                        # Prevedeni GPS casu a datumu do datetime
                        utc_time = utc_time[:-3]
                        time = ':'.join(a+b for a,b in zip(utc_time[::2], utc_time[1::2]))
                        date = '/'.join(a+b for a,b in zip(date_stamp[::2], date_stamp[1::2]))

                        log_datetime = datetime.datetime.strptime(date+" "+time, '%d/%m/%y %H:%M:%S')

                        # Update casu v RTC
                        rtc_modul.write_datetime(log_datetime)

                        # Zapsani logu
                        make_log(rtc_modul.read_datetime().strftime("%m/%d/%Y,%H:%M:%S")+":"+latitude+","+longitude+"\n", RUN_FOLDER_PATH+LOG_FILE_NAME)

                        last_log_datetime = datetime.datetime.now()

                    print("Airplane in flight")

                else:
                    # Pokud letadlo behem letu spomali pod minimalni rychlost,tak zacina odpocet ukonceni letu
                    decision_datetime = datetime.datetime.now()

                    print("Speed decreasing, landing time started")

            else:
                # Pokud se ve stavu ukoncovani letu zvysi rychlost, tak se pokracuje ve stavu letu
                if (knots > START_DECISION_SPEED_KNOTS):
                    decision_datetime = None

                    print("Speed incresed, landing timer stoped")
                    continue

                # Pokud je rychlost mensi nez letova rychlost po dobu x sekund, tak je let ukoncen
                if (datetime.datetime.now() - decision_datetime).total_seconds() > STOP_DELAY_SECONDS:
                    in_flight = False
                    decision_datetime = None

                    # Ulozeni logu do database
                    save_logfile_to_database(RUN_FOLDER_PATH+LOG_FILE_NAME)

                    # Uchovani vybraneho pilota pro dalsi let
                    if actual_pilot != '':
                        make_log("Pilot:"+actual_pilot+"\n", RUN_FOLDER_PATH+LOG_FILE_NAME)

                    print("Flight ended, file saved to database")

        else:
            if decision_datetime == None:
                # Letadlo navysilo rychlost letu nebo se zvysili otacky motoru, ze stavu na zemi zacina odpocet na urceni zda zacal let
                if (knots > START_DECISION_SPEED_KNOTS) or (motor_resolutions > FLIGHT_START_REVOLUTIONS):
                    decision_datetime = datetime.datetime.now()
                    print("Airspeed/revolutions alive, decision time started")

                else:
                    print("Airplane on ground")

            else:
                # Rychlost letadla a otacky motoru jsou pod minimalni hranici, stav vzletu zrusen
                if (knots < START_DECISION_SPEED_KNOTS) and (motor_resolutions < FLIGHT_START_REVOLUTIONS):
                    decision_datetime = None

                    print("Take off speed decresed, take off timer stoped")
                    continue

                # Pokud jsou rychlost letu nebo otacky motoru stabilni po dobu x sekund, tak je letadlo ve stavu letu
                if (datetime.datetime.now() - decision_datetime).total_seconds() > START_DELAY_SECONDS:
                    in_flight = True
                    decision_datetime = None

                    print("Flight started")
finally:
    serialPort.close()
    GPIO.cleanup()
    conn.close()

# end flight_recorder_process.py